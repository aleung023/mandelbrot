//
//  MandelbrotViewController.m
//  Mandelbrot
//
//  References:
//  Image Processing in iOS Part 1: Raw Bitmap Modification
//  https://www.raywenderlich.com/2335-image-processing-in-ios-part-1-raw-bitmap-modification
//
//  My Christmas Gift: Mandelbrot Set Computation In Python
//  https://www.ibm.com/developerworks/community/blogs/jfp/entry/My_Christmas_Gift?lang=en
//
//  Created by Albert Leung on 12/4/18.
//  Copyright © 2018 Whifflebird. All rights reserved.
//

#import "MandelbrotViewController.h"

#define Mask8(x) ( (x) & 0xFF )
#define R(x) ( Mask8(x) )
#define G(x) ( Mask8(x >> 8 ) )
#define B(x) ( Mask8(x >> 16) )
#define A(x) ( Mask8(x >> 24) )
#define RGBAMake(r, g, b, a) ( Mask8(r) | Mask8(g) << 8 | Mask8(b) << 16 | Mask8(a) << 24 )

@interface MandelbrotViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (assign, nonatomic) long double scale;
@property (assign, nonatomic) CGPoint origin;
@property (assign, nonatomic) CGPoint pinchOrigin;
@property (assign, nonatomic) NSInteger iterations;
@property (assign, nonatomic) long double bounds;
@property (assign, nonatomic) UInt32 brightness;
@property (assign, nonatomic) UInt32 contrast;
@property (assign, nonatomic) CGPoint initialGestureCenter;
@property (assign, nonatomic) CGPoint maxTranslation;

@end

@implementation MandelbrotViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.maxTranslation = CGPointMake(15.0, 15.0);
    
    [self start];
 
    [self zoom];
}

/// Things start to get hazy at scale = 0.00000000000002629063007850326362 for double
/// Things start to get hazy at scale = 0.00000000000000001080908665140106 for long double
- (void)zoom {
    [NSTimer scheduledTimerWithTimeInterval:0.01 repeats:YES block:^(NSTimer *timer) {
        [self paintMandelbrot];
        NSLog(@"scale: %.32Lf", self.scale);
        CGFloat scale = self.scale * 0.95;
        self.origin = [self calculateOrigin:scale];
        self.scale = scale;
    }];
}

- (void)start {
    self.scale = 5.0;
    self.origin = [self calculateOrigin:self.scale];
    self.pinchOrigin = self.origin;
    self.iterations = 5000;
    self.bounds = 3.0;
    self.brightness = 10;
    self.contrast = 20;
}

- (CGPoint)calculateOrigin:(CGFloat)scale {
    // Change the shift in the origin due to change in scale
    CGFloat width = CGRectGetWidth(self.imageView.frame);
    CGFloat height = CGRectGetHeight(self.imageView.frame);
    CGFloat x = scale * 0.5 + M_PI/2.0;
    CGFloat y = 0.5 * scale * height/width;
    return CGPointMake(x, y);
}

- (void)paintMandelbrot {
    NSUInteger width = CGRectGetWidth(self.imageView.frame);
    NSUInteger height = CGRectGetHeight(self.imageView.frame);
    
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    
    UInt32 *pixels = (UInt32 *)calloc(height * width, sizeof(UInt32));
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);

    UInt32 *currentPixel = pixels;
    
    long double rangeA = self.scale;
    long double rangeB = self.scale * (long double)height/(long double)width;
    for (NSUInteger y = 0; y < height; y++) {
        for (NSUInteger x = 0; x < width; x++) {
            long double a = rangeA * (long double)x/(long double)width - self.origin.x;
            long double b = rangeB * (long double)y/(long double)height - self.origin.y;
            UInt32 m = [self mandelbrotWitha:a b:b maxiter:self.iterations bounds:self.bounds] * self.contrast;
            UInt32 red = [self word:m order:0] * self.brightness;
            UInt32 green = [self word:m order:1] * self.brightness;
            UInt32 blue = [self word:m order:2] * self.brightness;
            // Clamp these values between 0 and 255
            red = MAX(0, MIN(255, red));
            green = MAX(0, MIN(255, green));
            blue = MAX(0, MIN(255, blue));
            *currentPixel = RGBAMake(red, green, blue, 255);
            currentPixel++;
        }
    }
    
    CGImageRef newCGImage = CGBitmapContextCreateImage(context);
    UIImage *processedImage = [UIImage imageWithCGImage:newCGImage];
    self.imageView.image = processedImage;
    CGImageRelease(newCGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    free(pixels);
}

- (UInt32)mandelbrotWitha:(long double)a b:(long double)b maxiter:(NSInteger)maxiter bounds:(long double)bounds {
    long double za = a;
    long double zb = b;
    for (UInt32 n = 0; n < maxiter; n++) {
        long double aa = za * za - zb * zb;
        long double bb = 2.0 * za * zb;
        za = aa + a;
        zb = bb + b;
        if (fabsl(za) > bounds || fabsl(zb) > bounds) {
            return n;
        }
    }
    return 0;
}

/*
 * b doesn't get bigger than 4835
 */
- (UInt32)word:(UInt32)b order:(int)order {
    b = b >> (4 * order);
    b = b & 0xf;
    return b;
}

- (IBAction)panGesture:(UIPanGestureRecognizer *)sender {
    return;
    /*
    UIView *piece = sender.view;
    CGPoint translation = [sender translationInView:piece.superview];
    CGPoint newCenter;
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            self.initialGestureCenter = piece.center;
            return;
        case UIGestureRecognizerStateCancelled:
            return;
        default:
            // Pin the translation speed
            if (fabs(translation.x) > self.maxTranslation.x) {
                translation.x = translation.x >= 0.0 ? self.maxTranslation.x : -self.maxTranslation.x;
            }
            if (fabs(translation.y) > self.maxTranslation.y) {
                translation.y = translation.y >= 0.0 ? self.maxTranslation.y : -self.maxTranslation.y;
            }
            newCenter = CGPointMake(self.origin.x + translation.x/300.0, self.origin.y + translation.y/300.0);
            self.origin = newCenter;
            NSLog(@"pan origin (%f, %f)", self.origin.x, self.origin.y);
            break;
    }
    [self paintMandelbrot]; */
}

- (IBAction)pinchGesture:(UIPinchGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged) {
        return;
        /*
        CGPoint originBeforeScale = [self calculateOrigin:5.0]; // Origin before any scaling
        CGFloat xOffset = self.origin.x - originBeforeScale.x; // This may not be correct because this origin is from scome scaling.
        CGFloat yOffset = self.origin.x - originBeforeScale.y;
        CGFloat scale = self.scale/sender.scale;
        CGPoint origin = [self calculateOrigin:scale];
        CGPoint newOrigin = CGPointMake(origin.x + xOffset, origin.y + yOffset);
        self.origin = newOrigin;
        self.scale = scale;
        NSLog(@"pinch origin (%f, %f)", self.origin.x, self.origin.y);
        [self paintMandelbrot]; */
    }
}

- (IBAction)tapGesture:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self start];
    }
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return NO;
}


@end
