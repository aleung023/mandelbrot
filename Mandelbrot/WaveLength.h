//
//  WaveLength.h
//  Mandelbrot
//
//  Created by Albert Leung on 2/24/19.
//  Copyright © 2019 Whifflebird. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WaveLength : NSObject

+ (NSArray *)rgbFromWavelength:(NSInteger)wavelength;

@end

NS_ASSUME_NONNULL_END
