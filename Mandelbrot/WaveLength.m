//
//  WaveLength.m
//  Mandelbrot
//
//  Created by Albert Leung on 2/24/19.
//  Copyright © 2019 Whifflebird. All rights reserved.
//

#import "WaveLength.h"

@implementation WaveLength

static double Gamma = 0.80;
static double IntensityMax = 255;

/** Taken from Earl F. Glynn's web page:
 * The visible spectrum is between 380 and 780 nm
 * <a href="http://www.efg2.com/Lab/ScienceAndEngineering/Spectra.htm">Spectra Lab Report</a>
 * */
+ (NSArray *)rgbFromWavelength:(NSInteger)waveLength {
    double factor;
    double red, green, blue;
    
    if ((waveLength >= 380) && (waveLength < 440)) {
        red = -(waveLength - 440) / (440 - 380);
        green = 0.0;
        blue = 1.0;
    } else if ((waveLength >= 440) && (waveLength < 490)) {
        red = 0.0;
        green = (waveLength - 440) / (490 - 440);
        blue = 1.0;
    } else if ((waveLength >= 490) && (waveLength < 510)) {
        red = 0.0;
        green = 1.0;
        blue = -(waveLength - 510) / (510 - 490);
    } else if ((waveLength >= 510) && (waveLength < 580)) {
        red = (waveLength - 510) / (580 - 510);
        green = 1.0;
        blue = 0.0;
    } else if ((waveLength >= 580) && (waveLength < 645)) {
        red = 1.0;
        green = -(waveLength - 645) / (645 - 580);
        blue = 0.0;
    } else if ((waveLength >= 645) && (waveLength < 781)) {
        red = 1.0;
        green = 0.0;
        blue = 0.0;
    } else {
        red = 0.0;
        green = 0.0;
        blue = 0.0;
    }
    
    // Let the intensity fall off near the vision limits
    
    if ((waveLength >= 380) && (waveLength < 420)) {
        factor = 0.3 + 0.7*(waveLength - 380) / (420 - 380);
    } else if ((waveLength >= 420) && (waveLength < 701)) {
        factor = 1.0;
    } else if ((waveLength >= 701) && (waveLength < 781)) {
        factor = 0.3 + 0.7*(780 - waveLength) / (780 - 700);
    } else {
        factor = 0.0;
    }
    
    // Don't want 0^x = 1 for x <> 0
    UInt32 r = red == 0.0 ? 0 : (UInt32)round(IntensityMax * pow(red * factor, Gamma));
    UInt32 g = green == 0.0 ? 0 : (UInt32)round(IntensityMax * pow(green * factor, Gamma));
    UInt32 b = blue == 0.0 ? 0 : (UInt32)round(IntensityMax * pow(blue * factor, Gamma));
    
    NSArray *array = [[NSArray alloc] initWithObjects:@(r), @(g), @(b), nil];
    
    return array;
}

@end
