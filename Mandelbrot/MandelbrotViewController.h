//
//  MandelbrotViewController.h
//  Mandelbrot
//
//  Created by Albert Leung on 12/4/18.
//  Copyright © 2018 Whifflebird. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MandelbrotViewController : UIViewController <UIGestureRecognizerDelegate>

@end

NS_ASSUME_NONNULL_END
